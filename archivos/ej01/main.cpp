#include <iostream>
#include <cstdlib>
using namespace std;

int main () {
	int legajo = 0;
	int nota = 0;
	float acumulador = 0;

	FILE *ptr = NULL;
	ptr = fopen("ej01.txt", "w");

	if (ptr == NULL) {
		printf("Error al abrir el archivo\n");
		exit(EXIT_FAILURE);
	}

	while (true) {
		acumulador = 0;

		printf("Ingrese el legajo: ");
		scanf("%d", &legajo);

		if (legajo < 0) {
			break;
		}

		for (int i = 0; i < 2; i++) {
			printf("Ingrese la nota %d: ", i+1);
			scanf("%d", &nota);
			acumulador += nota;
		}

		fprintf(ptr, "%d\t%4.2f\n", legajo, acumulador / 2);
	}

	fclose(ptr);
	return 0;
}
