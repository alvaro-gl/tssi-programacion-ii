#include <iostream>
#include <cstdlib>
using namespace std;

int main () {
	FILE *input_ptr = NULL;
	FILE *outpt_ptr = NULL;

	input_ptr = fopen("../ej01/ej01.txt", "r");
	outpt_ptr = fopen("ej02.txt", "w");

	if (input_ptr == NULL) {
		printf("Error al abrir el archivo de entrada\n");
		exit(EXIT_FAILURE);
	}

	if (outpt_ptr == NULL) {
		printf("Error al abrir el archivo de salida\n");
		exit(EXIT_FAILURE);
	}

	fputs("Listado de alumnos aprobados\n", outpt_ptr);

	int legajo = 0;
	float promedio = 0;

	while (fscanf(input_ptr, "%d %f", &legajo, &promedio) != EOF) {
		if (promedio >= 6) {
			fprintf(outpt_ptr, "%d\t%4.2f\n", legajo, promedio);
		}
	}

	fclose(input_ptr);
	fclose(outpt_ptr);

	return 0;
}
