#include <iostream>
#include <cstring>
using namespace std;

class FechaNacimiento {
	private:
		int dia;
		int mes;
		int anio;
	public:
		// No tengo ganas de hacer verificaciones de bisiesto y mes y bla bla bla
		void setDia (int d) {
			dia = d;
		}
		int getDia () {
			return dia;
		}

		void setMes (int m) {
			mes = m;
		}
		int getMes () {
			return mes;
		}

		void setAnio (int a) {
			anio = a;
		}
		int getAnio () {
			return anio;
		}
};

class Alumno {
	private:
		char nombre[21];
		FechaNacimiento fecha;
	public:
		void setNombre (char *n) {
			strcpy(nombre, n);
		}
		char* getNombre () {
			return nombre;
		}
		void setFechaNacimiento (char *f) {
			int dia, mes, anio;
			sscanf(f, "%d/%d/%d", &dia, &mes, &anio);
			fecha.setDia(dia);
			fecha.setMes(mes);
			fecha.setAnio(anio);
		}
		char* getFechaNacimiento () {
			int dia, mes, anio;
			dia = fecha.getDia();
			mes = fecha.getMes();
			anio = fecha.getAnio();

			char buffer[11];

			sprintf(buffer, "%02d/%02d/%d", dia, mes, anio);

			char *ptr = buffer;

			return ptr;
		}
};

int main () {
	Alumno a;
	char nombre[] = "Juan Carlos";
	char *ptr;

	ptr = nombre;

	a.setNombre(ptr);
	printf("[INFO] El nombre del alumno es %s\n", a.getNombre());

	char fecha[] = "01/02/2003";
	ptr = fecha;
	a.setFechaNacimiento(ptr);
	printf("[INFO] La fecha de nacimiento del alumno es %s\n", a.getFechaNacimiento());
}
