#include <iostream>
#include <cstring>
#include <cstdlib>
using namespace std;

struct Fecha {
	int dia;
	int mes;
	int anio;
};

struct Inscripcion {
	char apenom[26];
	int legajo;
	int codigo_materia;
	Fecha fecha;
};

int main () {
	char apenom[26];
	char aux_anio[11];
	int legajo;
	int codigo_materia;
	int dia;
	int mes;
	int anio;

	FILE* ptr;
	char* ptr_ch;

	if ((ptr = fopen("ej06.dat", "wb")) == NULL) {
		printf("Error al abrir el archivo\n");
		exit(EXIT_FAILURE);
	}

	while (true) {
		// Resets
		legajo = 0;
		codigo_materia = 0;

		// Necesito poder tomar un "nombre nulo"
		printf("> Ingrese el nombre del alumno: ");
		fgets(apenom, 26, stdin);
		ptr_ch = apenom;

		if (strcmp(ptr_ch, "\n") == 0) {
			printf("[INFO] Fin del ciclo de carga\n");
			break;
		}

		// fgets inserta un newline extra
		*(ptr_ch + (int) strlen(ptr_ch) - 1) = '\0';

		// Descarto el newline con %*c
		while (legajo <= 0 || legajo > 99999999) {
			printf("> Ingrese el legajo del alumno: ");
			scanf("%d%*c", &legajo);
		}

		while (codigo_materia <= 0 || codigo_materia > 9999999) {
			printf("> Ingrese el codigo de materia: ");
			scanf("%d%*c", &codigo_materia);
		}

		printf("> Ingrese la fecha del examen (DD/MM/AAAA): ");
		scanf("%[^\n]%*c", aux_anio);
		ptr_ch = aux_anio;

		// Voy a suponer que la fecha va a estar bien puesta porque los del front son buena gente
		sscanf(ptr_ch, "%d/%d/%d", &dia, &mes, &anio);

		// Build
		Fecha f{dia, mes, anio};
		Inscripcion i;
		strcpy(i.apenom, apenom);
		i.legajo = legajo;
		i.codigo_materia = codigo_materia;
		i.fecha = f;

		// A guardar
		fwrite(&i, sizeof(Inscripcion), 1, ptr);
	}

	fclose(ptr);

	return 0;
}
