#include <iostream>
#include <cstdlib>
using namespace std;

struct Fecha {
	int dia;
	int mes;
	int anio;
};

struct Inscripcion {
	char apenom[26];
	int legajo;
	int codigo_materia;
	Fecha fecha;
};

int main () {
	int materia = 0;

	FILE* input_ptr;
	if ((input_ptr = fopen("../ej06/ej06.dat", "rb")) == NULL) {
		printf("Error al abrir el archivo de entrada\n");
		exit(EXIT_FAILURE);
	}

	FILE* outpt_ptr;
	if ((outpt_ptr = fopen("ej07.dat", "wb")) == NULL) {
		printf("Error al abrir el archivo\n");
		exit(EXIT_FAILURE);
	}

	Inscripcion i;
	int r = fread(&i, sizeof(Inscripcion), 1, input_ptr);

	if (r == 0) {
		printf("No hay registros\n");
	} else {
		printf("Ingrese el codigo de la materia: ");
		scanf("%d", &materia);
	}

	while (!feof(input_ptr)) {
		if (i.codigo_materia == materia) {
			fwrite(&i, sizeof(Inscripcion), 1, outpt_ptr);
		}

		fread(&i, sizeof(Inscripcion), 1, input_ptr);
	}

	fclose(input_ptr);
	fclose(outpt_ptr);
	return 0;
}
