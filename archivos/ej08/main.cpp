#include <iostream>
#include <cstdlib>
using namespace std;

struct Fecha {
	int dia;
	int mes;
	int anio;
};

struct Inscripcion {
	char apenom[26];
	int legajo;
	int codigo_materia;
	Fecha fecha;
};

int main () {
	FILE* input_ptr;
	if ((input_ptr = fopen("../ej07/ej07.dat", "rb")) == NULL) {
		printf("Error al abrir el archivo de entrada\n");
		exit(EXIT_FAILURE);
	}

	FILE* outpt_ptr;
	if ((outpt_ptr = fopen("ej08.txt", "w")) == NULL) {
		printf("Error al abrir el archivo de salida\n");
		exit(EXIT_FAILURE);
	}

	Inscripcion i;
	int r = fread(&i, sizeof(Inscripcion), 1, input_ptr);

	if (r == 0) {
		printf("No hay registros\n");
		fclose(input_ptr);
		fclose(outpt_ptr);
		exit(EXIT_SUCCESS);
	} else {
		fprintf(outpt_ptr, "Legajo\t\tNombre y Apellido\t\tFecha\t\tCodigo de Materia\n");
	}

	while (!feof(input_ptr)) {
		fprintf(outpt_ptr, "%d\t\t%s\t\t\t%d/%d/%d\t%d\n", i.legajo, i.apenom, i.fecha.dia, i.fecha.mes, i.fecha.anio, i.codigo_materia);
		fread(&i, sizeof(Inscripcion), 1, input_ptr);
	}

	fclose(input_ptr);
	fclose(outpt_ptr);

	return 0;
}
