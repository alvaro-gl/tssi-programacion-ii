#include <iostream>
#include <cstdlib>
#include <cstring>

struct Fecha {
	int dia;
	int mes;
	int anio;
};

struct Inscripcion {
	char apenom[26];
	int legajo;
	int codigo_materia;
	Fecha fecha;
};

struct Alumno {
	int legajo;
	char apenom[26];
};

int main () {
	// Otro ejercicio de mierda.
	// En la cursada le agregan imprimir el resultado pero la verdad aburre mucho todo.

	FILE* input_ptr;
	if ((input_ptr = fopen("../ej07/ej07.dat", "rb")) == NULL) {
		printf("Error al abrir el archivo de entrada");
		exit(EXIT_FAILURE);
	}

	FILE* outpt_ptr;
	if ((outpt_ptr = fopen("ej10.dat", "wb")) == NULL) {
		printf("Error al abrir el archivo de salida");
		exit(EXIT_FAILURE);
	}

	Inscripcion i;

	int r = fread(&i, sizeof(Inscripcion), 1, input_ptr);

	while (!feof(input_ptr)) {
		fseek(outpt_ptr, sizeof(Alumno) * (i.legajo - 80001), SEEK_SET);

		Alumno a;
		a.legajo = i.legajo;
		strcpy(a.apenom, i.apenom);

		fwrite(&a, sizeof(Alumno), 1, outpt_ptr);

		fread(&i, sizeof(Inscripcion), 1, input_ptr);
	}

	fclose(input_ptr);
	fclose(outpt_ptr);

	// Testing
	input_ptr = fopen("ej10.dat", "rb");
	fseek(input_ptr, sizeof(Alumno) * 9, SEEK_SET);
	Alumno j;
	fread(&j, sizeof(Alumno), 1, input_ptr);
	printf("El nombre del alumno es %s\n", j.apenom);
	fclose(input_ptr);
}
