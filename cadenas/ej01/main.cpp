#include <iostream>
#include <cstring>
using namespace std;

int main () {
	char cadena[80 + 1];

	printf("Ingrese una cadena: ");
	scanf("%s", cadena);

	bool pal = true;
	int len = (int) strlen(cadena);

	// Caso base de un solo char
	if (len > 1) {
		for (int i = 0; i < len / 2; i++) {
			if (*(cadena + i) != *(cadena + len - 1 - i)) {
				pal = false;
			}
		}
	}

	if (pal) {
		printf("Es palindromo\n");
	} else {
		printf("No es palindromo\n");
	}

	return 0;
}
