#include <iostream>
#include <cstring>
using namespace std;

int main () {
	bool pal = true;
	char cadena[80 + 1];
	char *ptr;

	printf(">> Ingrese una cadena: ");
	scanf("%s", cadena);

	int lenCadena = (int) strlen(cadena);
	int mitad = lenCadena / 2;

	ptr = cadena;

	// Primera mitad
	char mitadA[mitad];
	strncpy(mitadA, ptr, mitad);

	if (lenCadena % 2 == 0) {
		ptr += mitad;
	} else {
		ptr = ptr + mitad + 1;
	}

	// Segunda mitad
	char mitadB[mitad];
	strncpy(mitadB, ptr, mitad);

	// Inversa de la segunda mitad
	char inversa[mitad];
	int j = 0;
	for (int i = mitad - 1; i >= 0; i--) {
		inversa[j] = mitadB[i];
		j++;
	}

	// Comparacion
	if (strncmp(mitadA, inversa, mitad) != 0) {
		pal = false;
	}

	if (pal) {
		printf("Es palindromo\n");
	} else {
		printf("No es palindromo\n");
	}
}
