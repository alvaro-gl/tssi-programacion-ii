#include <iostream>
#include <cstring>
#include <cmath>
using namespace std;

int main () {
	char buf[15 + 1];
	char aux[15 + 1];
	char *ptr;

	bool avanzar = false;

	// Usando scanf("%s", ipa) no puedo capturar una cadena vacia
	printf(">>> Ingrese una cadena para testear: ");
	scanf("%[^\n]%*c", buf);

	// Primer check
	// - Cadena sin puntos: si strtok encuentra aunque sea un delimiter, aux cambia su strlen a la cantidad de chars antes del delimiter.
	// - Cadena vacia.
	// - Cadena con menos de 7 caracteres.
	// Gotcha: si la cadena termina con un punto el contador sigue siendo 4

	strcpy(aux, buf);

	int lenb = (int) strlen(aux);

	ptr = strtok(aux, ".");

	int lena = (int) strlen(aux);

	if (lenb == lena || ptr == NULL || lenb < 7 || *(buf + lenb - 1) == '.') {
		printf(">> No es una direccion IP\n");
	} else {
		avanzar = true;
	}

	// Segundo check
	// - Estructura de cuatro octetos separados por tres puntos
	// - Que no termine con un punto
	if (avanzar) {
		int contador = 0;

		while (ptr != NULL) {
			contador++;
			ptr = strtok(NULL, ".");
		}

		if (contador != 4) {
			avanzar = false;
			printf(">> No es una direccion IP\n");
		}
	}

	// Tercer check, cada seccion es un octeto
	if (avanzar) {
		strcpy(aux, buf);
		ptr = strtok(aux, ".");

		while (avanzar && ptr != NULL) {
			int value = 0;

			if ((int) strlen(ptr) > 3){
				printf(">> No es una direccion IP\n");
				avanzar = false;
				break;
			} else {
				int len = (int) strlen(ptr);

				switch (len) {
					// Token solo tiene un caracter
					case 1: {
							// Aca empece a extrañar meter funciones
							// Descarto el caso si el caracter no es un digito (hay que ver que tan portable es esto)
							if (*ptr < 48 || *ptr > 57) {
								printf(">> No es una direccion IP\n");
								avanzar = false;
								break;
							} else {
								value += *ptr - 48;
								break;
							}
					}
					// Token tiene dos o tres caracteres
					default: {
							if (*ptr < 48 || *ptr > 57) {
								printf(">> No es una direccion IP\n");
								avanzar = false;
								break;
							} else {
								for (int i = len; i > 0; i--) {
									value += (*ptr - 48) * ((int) pow(10, i - 1));
									ptr += 1;
								}
								break;
							}
					}
				}
			}
			if (value >= 0 && value <= 255) {
				ptr = strtok(NULL, ".");
			} else {
				printf(">> No es una direccion IP\n");
				avanzar = false;
				break;
			}
		}

	}

	if (avanzar) {
		printf(">> Es una direccion IP\n");
	}
	return 0;
}
