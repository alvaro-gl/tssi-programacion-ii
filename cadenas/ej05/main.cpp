#include <iostream>
#include <cstring>
using namespace std;

int main () {
	char input[80 + 1];
	char output[80 + 1];
	char *ptr;

	printf(">> Ingrese una cadena: ");
	scanf("%[^\n]%*c", input);

	ptr = output;

	int j = 0;

	for (int i = (int) strlen(input) - 1; i >= 0; i--) {
		*(ptr + j) = *(input + i);
		j++;
	}

	printf("> Input: %s\n", input);
	printf("> Output: %s\n", output);

	return 0;
}
