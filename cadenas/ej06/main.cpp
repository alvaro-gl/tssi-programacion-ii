#include <iostream>
#include <cstring>
#include <cstdio>
using namespace std;

// DISCLAIMER: gets() esta deprecado y ya no existe en C++14, move on kids

int main () {
	char input[80 + 1];
	char palabra[40 + 1];
	char *ptr_a;
	char *ptr_b;

	printf(">> Ingrese una cadena o frase: ");
	fgets(input, 81, stdin);

	ptr_a = input;
	ptr_b = palabra;

	// La deteccion de palabras se basa en encontrar el final de una seguido de un espacio seguido del comienzo de otra.
	// Al final se agrega uno mas al contador para contabilizar la ultima palabra.
	int cantidad_de_palabras = 0;
	for (int i = 0; *(ptr_a + i + 2) != '\0'; i++) {
		if (*(ptr_a) != ' ' && *(ptr_a + i + 1) == ' ' && (*(ptr_a + i + 2) != ' ' && *(ptr_a + i + 2) != '\0')) {
			cantidad_de_palabras++;
		}
	}

	cantidad_de_palabras++;
	printf("> Cantidad de palabras: %d\n", cantidad_de_palabras);

	int indice = 0;
	int inicio_palabra = 0;
	int contador_letra = 0;
	int contador_palabra = 0;
	int contador_larga = 0;

	while (*(ptr_a + indice) != '\0') {
		if (*(ptr_a + indice) != ' ' && *(ptr_a + indice + 1) != '\0') {
			contador_letra++;
		} else {
			if (contador_palabra == 0 || contador_letra > contador_larga) {
				inicio_palabra = indice - contador_letra;
				contador_larga = contador_letra;

				for (int i = 0; i < contador_letra; i++) {
					*(ptr_b + i) = *(ptr_a + inicio_palabra + i);
				}

				*(ptr_b + contador_letra) = '\0';
			}

			contador_letra = 0;
			contador_palabra++;
		}
		indice++;
	}

	printf("> La palabra mas larga fue %s\n", palabra);

	return 0;
}
