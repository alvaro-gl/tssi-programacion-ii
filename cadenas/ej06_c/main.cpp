#include <iostream>
#include <cstring>
using namespace std;

int main () {
	char cadena[80+1];
	char *ptr;
	char *larga;

	printf(">> Ingrese una frase o cadena: ");
	scanf("%[^\n]%*c", cadena);

	ptr = strtok(cadena, " ");

	int contador_palabra = 0;
	int contador_letra = 0;
	while (ptr != NULL) {
		if (contador_palabra == 0 || (int) strlen(ptr) > contador_letra) {
			contador_letra = (int) strlen(ptr);
			larga = ptr;
		}

		ptr = strtok(NULL, " ");
		contador_palabra++;
	}

	printf("> La cantidad de palabras es: %d\n", contador_palabra);
	printf("> La palabra mas larga fue \"%s\" y tuvo %d letras\n", larga, contador_letra);
	return 0;
}
