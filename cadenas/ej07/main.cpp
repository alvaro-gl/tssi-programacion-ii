#include <iostream>
#include <cstring>
using namespace std;

bool esVocal (char c) {
	bool r = false;

	if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' ||
		c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U') {
		r = true;
	}

	return r;
}

int main () {
	char cadena[80+1];
	char output[80+1];
	char *ptr_p;
	char *ptr_n;

	printf("> Ingrese una frase: ");
	scanf("%[^\n]%*c", cadena);

	ptr_p = strtok(cadena, " ");
	ptr_n = strtok(NULL, " ");

	// Como el enunciado apesta voy a suponer que el algoritmo trabaja de a dos palabras
	if (ptr_n == NULL) {
		printf("> %s\n", ptr_p);
	} else {
		int iter = 0;
		while (ptr_n != NULL) {
			if (iter == 0) {
				strcpy(output, ptr_p);
				iter++;
			} else {
				strncat(output, ptr_p, (int) strlen(ptr_p));
			}

			if (! esVocal(*(ptr_p + (int) strlen(ptr_p) - 1)) && esVocal(*(ptr_n))) {
				strncat(output, ptr_n, (int) strlen(ptr_n));
				strncat(output, " \0", 2);
			} else {
				strncat(output, " \0", 2);
				strncat(output, ptr_n, (int) strlen(ptr_n));
				strncat(output, " \0", 2);
			}


			ptr_p = strtok(NULL, " ");
			ptr_n = strtok(NULL, " ");
		}
	}

	printf("> %s\n", output);

	return 0;
}
