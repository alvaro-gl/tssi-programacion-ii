#include <iostream>
#include <cstring>
using namespace std;

int main () {
	char cadena_a[80 + 1];
	char cadena_b[80 + 1];
	char output[80 + 1];

	char *ptr_a;
	char *ptr_o;

	printf(">> Ingrese la primera cadena: ");
	scanf("%[^\n]%*c", cadena_a);

	printf(">> Ingrese la segunda cadena: ");
	scanf("%[^\n]%*c", cadena_b);

	ptr_a = strtok(cadena_a, " ");

	int iter = 0;
	while (ptr_a != NULL) {
		ptr_o = strstr(cadena_b, ptr_a);

		if (ptr_o != NULL) {
			if (iter == 0) {
				strcpy(output, ptr_a);
				strncat(output, " \0", 2);
				iter++;
			} else {
				strncat(output, ptr_a, (int) strlen(ptr_a));
				strncat(output, " \0", 2);
			}
		}
		ptr_a = strtok(NULL, " ");
	}

	printf("> Resultado: %s\n", output);

	return 0;
}
