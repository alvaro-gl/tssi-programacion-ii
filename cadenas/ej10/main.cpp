#include <cmath>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;

int main () {
	char num_a[80+1];
	char num_b[80+1];
	char *ptr;

	long double n_a = 0;
	long double n_b = 0;

	printf(">> Ingrese la primera cifra: ");
	scanf("%[^\n]%*c", num_a);

	printf(">> Ingrese la segunda cifra: ");
	scanf("%[^\n]%*c", num_b);

	int len = (int) strlen(num_a);

	int indice = 0;

	for (int i = len - 1; i >= 0; i--) {
		n_a += (*(num_a + indice) - 48) * pow(10, i);
		indice++;
	}

	// As lazy as it gets
	len = (int) strlen(num_b);
	indice = 0;
	for (int i = len - 1; i >= 0; i--) {
		n_b += (*(num_b + indice) - 48) * pow(10, i);
		indice++;
	}

	printf("> Valor de la primera cifra: %Le\n", n_a);
	printf("> Valor de la segunda cifra: %Le\n", n_b);

	printf("> La suma es: %Le\n", n_a + n_b);

	return 0;
}
