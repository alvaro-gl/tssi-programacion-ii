#include <iostream>
using namespace std;

int main () {
	int entero = 10;
	char caracteres[10] = "Ale Gato!";
	float flotante = 5;

	printf("La direccion de la variable entero es %p\n", &entero);
	printf("La direccion de la variable caracteres es %p\n", &caracteres);
	printf("La direccion de la variable flotante es %p\n", &flotante);

	return 0;
}
