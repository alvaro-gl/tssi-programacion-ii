#include <iostream>
using namespace std;

int main () {
	int *punteroEntero;

	printf("[a] La direccion de memoria del puntero a entero es %p\n", &punteroEntero);
	printf("[b] El valor de la posicion de memoria a la que apunta punteroEntero es %d\n", *punteroEntero);

	// [c] Asignar dinamicamente espacio en memoria
	int *otroPuntero;
	printf("[d] El valor de la posicion de memoria a la que apunta punteroEntero es %d\n", *punteroEntero);
	printf("[e] El valor de la posicion de memoria a la que apunta otroPuntero es %d\n", *otroPuntero);
	return 0;
}
