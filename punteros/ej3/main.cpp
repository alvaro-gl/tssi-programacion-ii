#include <iostream>
using namespace std;

int main () {
	char cadena[10] = "hola";

	char *miPuntero;
	miPuntero = cadena;

	printf("[a] El valor de miPuntero como string es: %s\n", miPuntero);
	printf("[b] La direccion de memoria de miPuntero es: %p\n", &miPuntero);
	printf("[c] El valor en la posicion de memoria a la que apunta miPuntero es: %c\n", *miPuntero);
	printf("[d] El valor de miPuntero[0] que es lo mismo que obtendria con *miPuntero es: %c\n", miPuntero[0]);
	printf("[e] El valor de miPuntero[0] es: %c\n", miPuntero[0]);
	miPuntero++;
	printf("[e] El valor de miPuntero[0] luego de correr miPuntero++ es: %c\n", miPuntero[0]);
	printf("[e] La conclusion es que podemos referenciar elementos en un array aplicando aritmetica de punteros.\n");

	return 0;
}
