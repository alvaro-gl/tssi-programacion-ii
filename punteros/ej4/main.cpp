#include <iostream>
#include <cstring>
#include "funciones.h"
using namespace std;

int main () {
	Estruc e;

	// Asignacion
	strcpy(e.nombre, "Kokumo");
	e.edad = 30;

	printEstruc(e);

	printf("Mostrando el valor de nombre dentro del main: %s\n", e.nombre);
	printf("Mostrando el valor de edad dentro del main: %d\n", e.edad);
	printf("La conclusion es que pasando el struct como parametro la funcion obtiene una copia del struct original, por lo tanto las modificaciones no se reflejan en el struct local al main.\n");
	return 0;
}
