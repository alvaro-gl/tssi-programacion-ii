#include <iostream>
#include <cstring>
#include "estructuras.h"

void printEstruc(Estruc &e) {
	printf("Mostrando el valor de nombre dentro de la funcion: %s\n", e.nombre);
	printf("Mostrando el valor de edad dentro de la funcion: %d\n", e.edad);

	printf("Cambiando el valor de nombre dentro de la funcion...\n");
	strcpy(e.nombre, "Alejandro");
	printf("Cambiando el valor de edad dentro de la funcion...\n");
	e.edad = 40;

	printf("Mostrando el valor de nombre dentro de la funcion: %s\n", e.nombre);
	printf("Mostrando el valor de edad dentro de la funcion: %d\n", e.edad);
}
