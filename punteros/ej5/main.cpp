#include <iostream>
#include <cstring>
#include "funciones.h"
using namespace std;

int main () {
	Estruc e;

	// Asignacion
	strcpy(e.nombre, "Kokumo");
	e.edad = 30;

	printEstruc(e);

	printf("Mostrando el valor de nombre dentro del main: %s\n", e.nombre);
	printf("Mostrando el valor de edad dentro del main: %d\n", e.edad);
	printf("A diferencia del ejercicio 4, en este caso pasamos el struct por referencia, por lo tanto la funcion recibe la direccion del struct creado en el main y sus valores son modificados con exito.\n");
	return 0;
}
