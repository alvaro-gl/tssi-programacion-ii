#define MAX_ELEMENTOS 10
#include <iostream>

void inicializarVector (int a[MAX_ELEMENTOS]) {
	for  (int i = 0; i < MAX_ELEMENTOS; i++) {
		if (i == 0) {
			a[i] = 0;
		} else {
			a[i] = i * (i - 1);
		}
	}
}

void imprimirVector (int a[MAX_ELEMENTOS]) {
	for  (int i = 0; i < MAX_ELEMENTOS; i++) {
		printf("Posicion: %d - Valor: %d\n", i, a[i]);
	}
}

void imprimirVectorInvertido (int a[MAX_ELEMENTOS]) {
	for  (int i = MAX_ELEMENTOS - 1; i >= 0; i--) {
		printf("Posicion: %d - Valor: %d\n", i, a[i]);
	}
}
