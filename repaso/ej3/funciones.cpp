#define MAX_PERSONAS 10
#include "estructuras.h"
using namespace std;

void agregarPersona(Persona p[MAX_PERSONAS], int pos) {
	string nombre(100, ' ');
	int edad = 0;

	printf("Ingrese el nombre: ");
	scanf("%100s", &nombre[0]);
	p[pos].nombre = nombre;

	while (edad <= 0) {
		printf("Ingrese la edad: ");
		scanf("%d", &edad);

		if (edad <= 0) {
			printf("La edad ingresada no es valida!\n");
		} else {
			p[pos].edad = edad;
		}
	}
}

void imprimirPersona(Persona p[MAX_PERSONAS], int pos) {
	printf("Nombre: %s - Edad: %d\n", p[pos].nombre.c_str(), p[pos].edad);
}

void ordenarPersonasPorEdad(Persona p[MAX_PERSONAS], int cantidad) {
	int i = 0;
	int j;
	Persona aux;
	bool ordenado = false;

	while (i < cantidad && !ordenado) {
		ordenado = true;

		for (j = 0; j < cantidad - i - 1; j++) {
			if (p[j].edad < p[j + 1].edad) {
				aux = p[j];
				p[j] = p[j + 1];
				p[j + 1] = aux;
				ordenado = false;
			}
		}
		i++;
	}
}
