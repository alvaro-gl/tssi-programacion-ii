#include "estructuras.h"
#define MAX_PERSONAS 10

void agregarPersona(Persona p[MAX_PERSONAS], int pos);
void imprimirPersona(Persona p[MAX_PERSONAS], int pos);
void ordenarPersonasPorEdad(Persona p[MAX_PERSONAS], int cantidad);
