#include "funciones.h"
#define MAX_PERSONAS 10

int main () {
	Persona p[MAX_PERSONAS];
	int contador = 0;
	char c;

	printf("> Agregador de Personas\n");
	while (contador < MAX_PERSONAS) {
		printf("Desea ingresar una persona? (y/n): ");
		scanf(" %c", &c);

		if (c == 'n') {
			break;
		}

		agregarPersona(p, contador);
		contador++;
	}

	printf("> Resumen de personas agregadas\n");
	for (int i = 0; i < contador; i++) {
		imprimirPersona(p, i);
	}

	ordenarPersonasPorEdad(p, contador);
	printf("> Personas agregadas ordenadas por edad\n");
	for (int i = 0; i < contador; i++) {
		imprimirPersona(p, i);
	}

	return 0;
}
