#define MAX_EMPLEADOS 10
#define MAX_NOMBRE 100
#define MAX_TRAMITES 10
#include <iostream>
using namespace std;

struct Empleado {
	string nombre;
	int legajo;
};

struct TipoTramite {
	int tipo;
	string descripcion;
};

struct Tramite {
	Empleado e;
	TipoTramite t;
};
