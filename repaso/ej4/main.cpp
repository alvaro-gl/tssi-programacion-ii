#include "funciones.h"

int main () {
	Tramite t[5];
	crearEntornoTest(t, 5);

	printf("> Sistema de Administracion hecho con pocas ganas\n");

	printf(">> La primera persona atendidad en el dia fue %s\n", t[0].e.nombre.c_str());
	int bajo = legajoMasBajo(t, 5);

	printf(">> El legajo mas bajo fue el de %s: %d\n", t[bajo].e.nombre.c_str(), t[bajo].e.legajo);

	// Hice todo para testear las funciones importantes, contar elementos de un array es trivial
	printf(">> Total de personas: %d\n", 5);

	ordenarPorTipoDeTramite(t, 5);
	printf(">> Cantidad de tramites por tipo del dia:\n");
	cantidadTramitesPorTipo(t, 5);

	printf("\n>> Resumen del dia\n");
	mostrarResumen(t, 5);
	return 0;
}
