# Ejercicios de Geometría

DISCLAIMER: No se si esto era con structs o no así que los hice con clases que va a ser lo que deberíamos terminar haciendo en la materia.

La clase Figura es una interfaz o clase abstracta y las clases que hereden de esa interfaz tienen que implementar los métodos de área y perímetro. En cuanto a la esfera y el cilindro, heredan de circunferencia pero solo por una cuestión puramente geométrica, son consecuencia de pasar de dos a tres dimensiones.
