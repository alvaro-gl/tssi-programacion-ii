#include "estructuras.h"
#include <iostream>
using namespace std;

int main () {
	int opcion = 0;
	printf("> Sistema de geometria\n");
	printf(">>> [1] Cuadrado\n");
	printf(">>> [2] Circulo\n");
	printf(">>> [3] Triangulo Equilatero\n");
	printf(">>> [4] Pentagono Regular\n");
	printf(">>> [5] Esfera\n");
	printf(">>> [6] Cilindro\n");
	printf(">> Seleccione una opcion: ");
	scanf("%d", &opcion);

	switch (opcion) {
		case 1:
			{
				float lado = 0;
				printf("Ingrese el lado del cuadrado: ");
				scanf("%f", &lado);

				if (lado != 0) {
					Cuadrado c;
					c.setLado(lado);
					printf(">>> El area del cuadrado es %4.2f\n", c.area());
					printf(">>> El perimetro del cuadrado es %4.2f\n", c.perimetro());
				} else {
					printf("[ERROR] Debe ingresar el lado del cuadrado\n");
				}
				break;
			}
		case 2:
			{
				float radio = 0;
				printf("Ingrese el radio del circulo: ");
				scanf("%f", &radio);

				if (radio != 0) {
					Circulo c;
					c.setRadio(radio);
					printf(">>> El area del circulo es %4.2f\n", c.area());
					printf(">>> El perimetro del circulo es %4.2f\n", c.perimetro());
				} else {
					printf("[ERROR] Debe ingresar el radio del circulo\n");
				}
				break;
			}
		case 3:
			{
				float lado, base = 0;
				printf("Ingrese el lado del triangulo: ");
				scanf("%f", &lado);

				if (lado != 0) {
					printf("Ingrese la base del triangulo: ");
					scanf("%f", &base);

					if (base != 0) {
						TrianguloEquilatero t;
						t.setLado(lado);
						t.setBase(base);
						printf(">>> El area del triangulo es %4.2f\n", t.area());
						printf(">>> El perimetro del triangulo es %4.2f\n", t.perimetro());
					} else {
						printf("[ERROR] Debe ingresar la base del triangulo\n");
					}
				} else {
					printf("[ERROR] Debe ingresar el lado del triangulo\n");
				}
				break;
			}
		case 4:
			{
				float lado = 0;
				printf("Ingrese el lado del pentagono: ");
				scanf("%f", &lado);

				if (lado != 0) {
					Pentagono p;
					p.setLado(lado);
					printf(">>> El area del pentagono es %4.2f\n", p.area());
					printf(">>> El perimetro del pentagono es %4.2f\n", p.perimetro());
				} else {
					printf("[ERROR] Debe ingresar el lado del pentagono\n");
				}
				break;

			}
		case 5:
			{
				float radio = 0;
				printf("Ingrese el radio de la esfera: ");
				scanf("%f", &radio);

				if (radio != 0) {
					Esfera e;
					e.setRadio(radio);
					printf(">>> El volumen de la esfera es %4.2f\n", e.volumen());
				} else {
					printf("[ERROR] Debe ingresar el radio de la esfera\n");
				}
				break;
			}
		case 6:
			{
				float radio, altura = 0;
				printf("Ingrese el radio del cilindro: ");
				scanf("%f", &radio);

				if (radio != 0) {
					printf("Ingrese la altura del cilindro: ");
					scanf("%f", &altura);

					if (altura != 0) {
						Cilindro c;
						c.setRadio(radio);
						c.setAltura(altura);
						printf(">>> El volumen del cilindro es %4.2f\n", c.volumen());
					} else {
						printf("[ERROR] Debe ingresar la altura del cilindro\n");
					}
				} else {
					printf("[ERROR] Debe ingresar el radio del cilindro\n");
				}
				break;
			}
		default:
			printf("[ERROR] Opcion no valida\n");
	}
}
