#include <cstdlib>
#include <cstring>
#include <iostream>
#include "estructuras.h"
#define MIN_CARACTERES_PASSWORD 8

//void leerDat () {
//	FILE* ptr = NULL;
//	ptr = fopen("users.dat", "rb");
//
//	if (ptr == NULL) {
//		printf("Error al abrir users.dat\n");
//		exit(EXIT_FAILURE);
//	}
//
//	Usuario u;
//
//	int r = fread(&u, sizeof(Usuario), 1, ptr);
//
//	printf("Nombre: %s\n", u.nombre);
//
//	fclose(ptr);
//}

bool numeroCaracteresValidos(int maximo, char* cadena) {
	if (strlen(cadena) > maximo) {
		return false;
	} else {
		return true;
	}
}

bool nombreUsuarioValido (char* nombreUsuario) {
	return numeroCaracteresValidos(MAX_CARACTERES_NOMBRE_USUARIO, nombreUsuario);
}

bool emailUsuarioValido (char* emailUsuario) {
	bool r = false;
	if (numeroCaracteresValidos(MAX_CARACTERES_EMAIL_USUARIO, emailUsuario)) {
		int i = 0;
		while (*(emailUsuario + i) != '\0') {
			if (*(emailUsuario + i) == '@') {
				r = true;
			}
			i++;
		}
	}

	return r;
}

bool passwordUsuarioValida (char* passwd) {
	bool r = true;

	if (strlen(passwd) >= MIN_CARACTERES_PASSWORD) {
		int i = 0;
		while (*(passwd + i) != '\0') {
			if (int(*(passwd + i)) < 48) {
				r = false;
			}
			if (int(*(passwd + i)) > 57 && int(*(passwd + i)) < 65) {
				r = false;
			}
			if (int(*(passwd + i)) > 90 && int(*(passwd + i)) < 97) {
				r = false;
			}
			if (int(*(passwd + i)) > 122) {
				r = false;
			}
			i++;
		}
	} else {
		r = false;
	}

	return r;
}

void generarDat () {
	FILE* ptr_input = NULL;
	ptr_input = fopen("users.txt", "r");

	if (ptr_input == NULL) {
		printf("Error al abrir users.txt\n");
		exit(EXIT_FAILURE);
	}

	FILE* ptr_outpt = NULL;
	ptr_outpt = fopen("users.dat", "wb");

	if (ptr_outpt == NULL) {
		printf("Error al abrir users.dat\n");
		exit(EXIT_FAILURE);
	}

	FILE* ptr_rejtd = NULL;
	ptr_rejtd = fopen("rejected.txt", "w");

	if (ptr_rejtd == NULL) {
		printf("Error al abrir rejected.txt\n");
		exit(EXIT_FAILURE);
	}

	char nombre[MAX_CARACTERES_NOMBRE_USUARIO + 1];
	char email[MAX_CARACTERES_EMAIL_USUARIO + 1];
	char passwd[MAX_CARACTERES_PASSWORD + 1];

	// Mejor strtok
	int bTell;
	while ((bTell = ftell(ptr_input)) != -1 && (fscanf(ptr_input, "%[^;];%[^;];%[^\n]%*c", nombre, email, passwd) != EOF)) {
		if (nombreUsuarioValido(nombre) && emailUsuarioValido(email) && passwordUsuarioValida(passwd)) {
			nombre[strlen(nombre)] = '\0';
			email[strlen(email)] = '\0';
			passwd[strlen(passwd)] = '\0';

			Usuario u;
			strcpy(u.nombre, nombre);
			strcpy(u.email, email);
			strcpy(u.password, passwd);
			fwrite(&u, sizeof(Usuario), 1, ptr_outpt);
		} else {
			char linea[MAX_CARACTERES_LINEA];
			fseek(ptr_input, bTell, SEEK_SET);
			fgets(linea, MAX_CARACTERES_LINEA, ptr_input);
			fprintf(ptr_rejtd, "%s", linea);
		}
	}

	fclose(ptr_input);
	fclose(ptr_outpt);
	fclose(ptr_rejtd);
}

// Cargo el dat en memoria
int datToArray (Usuario* u) {
	int i = 0;

	FILE* ptr = NULL;
	ptr = fopen("users.dat", "rb");

	if (ptr == NULL) {
		printf("Error al abrir users.dat\n");
		exit(EXIT_FAILURE);
	}

	fread(&u[i], sizeof(Usuario), 1, ptr);
	i++;

	while (!feof(ptr)) {
		fread(&u[i], sizeof(Usuario), 1, ptr);

		if (!feof(ptr)) {
			i++;
		}
	}

	fclose(ptr);

	return i;
}

void arrayToDat (Usuario* u, int cantidad) {
	FILE* ptr = NULL;
	ptr = fopen("users.dat", "wb");

	if (ptr == NULL) {
		printf("Error al abrir users.dat\n");
		exit(EXIT_FAILURE);
	}

	for (int i = 0; i < cantidad; i++) {
		fwrite(&u[i], sizeof(Usuario), 1, ptr);
	}

	fclose(ptr);
}

void ordenarUsuarios () {
	Usuario u[MAX_USUARIOS];
	Usuario aux;

	char em1[MAX_CARACTERES_EMAIL_USUARIO + 1];
	char em2[MAX_CARACTERES_EMAIL_USUARIO + 1];
	char arroba[] = "@";

	int i = 0, j = 0, k = 0;
	bool ordenado = false;

	int cantidad = datToArray(u);

	if (cantidad >= 0 && cantidad <= 1) {
		// No hay mucho para hacer
	} else {
		while (i < cantidad && !ordenado) {
			ordenado = true;

			for (j; j < cantidad - i - 1; j++) {

				int charEm1 = strcspn(u[j].email, arroba);
				int charEm2 = strcspn(u[j + 1].email, arroba);

				strncpy(em1, u[j].email, charEm1);
				strncpy(em2, u[j+1].email, charEm2);
				em1[charEm1] = '\0';
				em2[charEm2] = '\0';

				int cmp = strcmp(em1, em2);
				if (cmp > 0) {
					aux = u[j+1];
					u[j+1] = u[j];
					u[j] = aux;
					ordenado = false;
				}
			}
			i++;
		}
	}

	arrayToDat (u, cantidad);
}

int buscarUsuarioPorMail (char* email) {
	Usuario u[MAX_USUARIOS];

	int i = datToArray(u);

	int inicio = 0;
	int fin = i - 1;

	while (fin >= inicio) {
		int mitad = inicio + (fin - inicio) / 2;

		// Si está en el medio devuelvo la pos
		if (strcmp(u[mitad].email, email) == 0) {
			return mitad;
		}

		if (strcmp(email, u[mitad].email) > 0) {
			inicio = mitad + 1;
		} else {
			fin = mitad - 1;
		}
	}

	return -1;
}

int buscarUsuarioPorNombre (char* nombre) {
	Usuario u[MAX_USUARIOS];

	int i = datToArray(u);

	int inicio = 0;
	int fin = i - 1;

	while (fin >= inicio) {
		int mitad = inicio + (fin - inicio) / 2;

		// Si está en el medio devuelvo la pos
		if (strcmp(u[mitad].nombre, nombre) == 0) {
			return mitad;
		}

		if (strcmp(nombre, u[mitad].nombre) > 0) {
			inicio = mitad + 1;
		} else {
			fin = mitad - 1;
		}
	}

	return -1;
}

bool login (char* email, char* passwd) {
	Usuario u[MAX_USUARIOS];
	bool autenticado = false;

	int i = buscarUsuarioPorMail(email);

	if (i != -1) {
		int j = datToArray(u);

		if (strcmp(u[i].password, passwd) == 0) {
			autenticado = true;
		}
	}

	return autenticado;
}

void agregarUsuario (char* nombre, char* email, char* passwd) {
	Usuario u[MAX_USUARIOS];

	Usuario a;
	strcpy(a.nombre, nombre);
	strcpy(a.email, email);
	strcpy(a.password, passwd);

	if (nombreUsuarioValido(nombre) && emailUsuarioValido(email) && passwordUsuarioValida(passwd)) {
		int i = datToArray(u);

		if (buscarUsuarioPorNombre(nombre) == -1 && buscarUsuarioPorMail(email) == -1) {
			u[i] = a;
			arrayToDat(u, i+1);
			ordenarUsuarios();
		} else {
			printf("Ya existe un usuario con ese nombre o con ese email\n");
		}
	} else {
		printf("Los datos ingresados no son validos.\n");
	}
}
